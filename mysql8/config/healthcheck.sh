#!/bin/bash
# 0: ready, 1: unhealthy, 2: reserved

if [ -f /var/lib/mysql/init-complete.txt ];
then
    mysqladmin --defaults-extra-file=/etc/mysql/conf.d/00-healthcheck.cnf ping
else
    exit 1
fi