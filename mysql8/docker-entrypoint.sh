#!/bin/bash

## MySQL Healthcheck
echo -e "[client]\nsocket = /var/run/mysqld/mysqld.sock\nuser = sysdb\npassword = $MYSQL_ADM_PASSWORD" > /etc/mysql/conf.d/00-healthcheck.cnf

## Init Script needs MySQL Server running 
if [ ! -f /var/lib/mysql/init-complete.txt ]; then
    echo "-------------------------------------------------------"
    echo "[INIT] STARTING TEMP MYSQL SERVER"

    mysqld --initialize-insecure --user=mysql
    mysqld_safe &
    
    # !!! Wait for MySQL Server Up
    sleep 30

    echo "[INIT] CREATING ADMINISTRATIVE MYSQL USER, 'sysdb'"
    mysql -u root --skip-password -e \
        "CREATE USER 'sysdb'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_ADM_PASSWORD';"
    mysql -u root --skip-password -e \
        "GRANT ALL PRIVILEGES ON *.* TO 'sysdb'@'%' WITH GRANT OPTION;"

    echo "[INIT] WRITING INIT COMPLETION FLAG"
    echo -e 'MySQL Server init Complete' > /var/lib/mysql/init-complete.txt
    date >> /var/lib/mysql/init-complete.txt
    chown mysql:mysql /var/lib/mysql/init-complete.txt

    mysqladmin -u root --skip-password shutdown
    echo "[INIT] STOPPING TEMP MYSQL SERVER"

    # !!! Wait for MySQL Server Down or KILL IT
    sleep 30
    pkill -9 mysqld*
    echo "-------------------------------------------------------"
fi

## Finally, Start services with Supervisord
/usr/bin/supervisord -c /etc/supervisor/supervisord.conf